class SceneEnd extends GuaScene {
    constructor(game) {
        super(game)
        game.registerAction('r', function(){
            var title = SceneTitle.new(game)
            game.replaceScene(title)
        })
    }

    draw() {
        // draw labels
        this.game.context.fillText('Game Over, press "R" to return to title', 200, 150)
    }

    update() {

    }
}