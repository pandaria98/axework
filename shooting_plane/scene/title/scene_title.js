class SceneTitle extends GuaScene {
    constructor(game) {
        super(game)
        this.setup()
    }

    setup() {
        var game = this.game
        var label = GuaLabel.new(game, 'press K to start')
        this.addElement('label', label)

        game.registerAction('k', function(){
            var main = Scene.new(game)
            game.replaceScene(main)
        })
    }
}
