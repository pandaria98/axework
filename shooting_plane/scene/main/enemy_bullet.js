class EnemyBullet extends GuaImage {
    constructor(game) {
        super(game, 'enemyBullet')
        this.setup()
    }
    
    setup() {
        this.speed = 10
        this.hit = false
        this.touchDown = false
    }
    
    debug() {
        this.speed = config.enemy_bullet_speed.value
    }

    collide(player) {
        return(rectIntersect(this, player))
    }

    hitPlayer() {
        log('hit player')
        this.hit = true
    }

    update() {
        this.y += this.speed
        
        // hits player
        for (const e of this.scene.elements['player']) {
            if (this.collide(e)) {
                this.hitPlayer()
                e.isHit()
            }
        }

        if (this.y > 900) {
            this.touchDown = true
        }
    }
}
