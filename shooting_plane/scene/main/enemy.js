class Enemy extends GuaImage {
    constructor(game) {
        var type = randomBetween(0, 4)
        var name = 'enemy' + type
        super(game, name)
        this.setup()
    }

    setup() {
        this.speed = randomBetween(2, 5)
        this.x = randomBetween(50, 470)
        this.y = -randomBetween(200, 300)
        this.health = 1
        this.cooldown = 60
    }

    isHit() {
        this.health -= 1
    }

    collide(player) {
        return(rectIntersect(this, player))
    }

    showParticle() {
        var ps = GuaParticleSystem.new(this.game)
        ps.x = this.x + this.w / 2
        ps.y = this.y + this.h / 2
        this.scene.addElement('particleSystem', ps)
    }

    die() {
        this.showParticle()
        this.setup()
    }

    setupBullet() {
        var b = EnemyBullet.new(this.game)
        b.x = this.x + this.w / 2 - b.w / 2
        b.y = this.y + this.h / 2
        this.scene.addElement('enemy_bullet', b)
    }

    fire() {
        if (this.cooldown == 0) {
            this.cooldown = config.enemy_fire_cooldown.value
            this.setupBullet()
        }
    }

    update() {
        this.y += this.speed
        if (this.y > 900) {
            this.setup()
        }

        var p = this.scene.player
        if (this.collide(p)) {
            p.isHit()
        }

        if (this.health <= 0) {
            this.die()
        }

        if (this.cooldown > 0) {
            this.cooldown -= 1
        } else if (this.cooldown == 0) {
            this.fire()
        }
    }
}