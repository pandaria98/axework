class Stone extends GuaImage {
    constructor(game) {
        super(game, 'stone0')
        this.setup()
    }

    setup() {
        this.speed = 1
    }

    debug() {
        this.speed = config.stone_speed.value
    }

    update() {
        this.y += this.speed
        if (this.y > 900) {
            this.setup()
        }
    }
}