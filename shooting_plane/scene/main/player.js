class Player extends GuaImage {
    constructor(game) {
        super(game, 'player')
        this.setup()
    }

    setup() {
        this.speed = 10
        this.cooldown = 0
        this.health = 1
        this.alive = true
    }

    keepInScreen() {
        var g = this.game
        var b = g.getCanvasBoundingBox()
        if (this.x < b.left) {
            this.x = b.left
        } else if (this.x + this.w > b.right) {
            this.x = b.right - this.w
        }
        if (this.y < b.top) {
            this.y = b.top
        } else if (this.y + this.h > b.bottom) {
            this.y = b.bottom - this.h
        }
    }

    update() {
        this.keepInScreen()
        if (this.cooldown > 0) {
            this.cooldown--
        }
        if (this.health <= 0) {
            this.die()
        }
    }

    debug() {
        this.speed = config.player_speed.value
    }

    setupBullet() {
        var b = Bullet.new(this.game)
        b.x = this.x + this.w / 2 - b.w / 2
        b.y = this.y
        this.scene.addElement('bullet', b)
    }

    fire() {
        if (this.cooldown == 0) {
            this.cooldown = config.fire_cooldown.value
            this.setupBullet()
        }
    }

    isHit() {
        this.health -= 1
    }

    die() {
        this.alive = false

        var ps = GuaParticleSystem.new(this.game)
        ps.x = this.x + this.w / 2
        ps.y = this.y + this.h / 2
        this.scene.addElement('particleSystem', ps)
    }

    moveRight() {
        this.x += this.speed
    }

    moveLeft() {
        this.x -= this.speed
    }

    moveUp() {
        this.y -= this.speed
    }

    moveDown() {
        this.y += this.speed
    }
}