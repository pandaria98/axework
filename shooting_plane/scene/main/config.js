config = {
    fps: {
        _comment: "FPS",
        value: 60,
    },
    player_speed: {
        _comment: "玩家速度",
        value: 10,
    },
    stone_speed: {
        _comment: "石头速度",
        value: 1,
    },
    bullet_speed: {
        _comment: "子弹速度",
        value: 20,
    },
    fire_cooldown: {
        _comment: "冷却时间",
        value: 9,
    },
    enemy_bullet_speed: {
        _comment: "敌人子弹速度",
        value: 6,
    },
    enemy_fire_cooldown: {
        _comment: "敌人子弹冷却时间",
        value: 100,
    },
}