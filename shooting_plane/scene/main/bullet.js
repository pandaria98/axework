class Bullet extends GuaImage {
    constructor(game) {
        super(game, 'bullet')
        this.setup()
    }
    
    setup() {
        this.speed = 20
        this.hit = false
    }
    
    debug() {
        this.speed = config.bullet_speed.value
    }

    collide(enemy) {
        return(rectIntersect(this, enemy))
    }

    hitEnemy() {
        log('hit')
        this.hit = true
    }

    update() {
        this.y -= this.speed
        
        // hits enemy
        for (const e of this.scene.elements['enemies']) {
            if (this.collide(e)) {
                this.hitEnemy()
                e.isHit()
            }
        }
    }
}
