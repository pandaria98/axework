class Scene extends GuaScene {
    constructor(game) {
        super(game)
        this.setup()
        this.setupInputs()
    }

    setup() {
        var game = this.game
        this.numberOfEnemies = 10
        this.bg = GuaImage.new(game, 'bg')
        this.stone = Stone.new(game)


        this.addElement('bg', this.bg)
        this.addElement('stone', this.stone)
        this.addPlayer()
        this.addEnemies()
    }

    setupInputs() {
        var s = this
        var g = this.game
        g.registerAction('a', function(){
            s.player.moveLeft()
        })
        g.registerAction('d', function(){
            s.player.moveRight()
        })
        g.registerAction('w', function(){
            s.player.moveUp()
        })
        g.registerAction('s', function(){
            s.player.moveDown()
        })
        g.registerAction('f', function(){
            s.player.fire()
        })
        g.registerAction('r', () => {
            if (!this.player.alive) {
                var s = Scene.new(this.game)
                this.game.replaceScene(s)
            }
        })
    }
    
    addPlayer() {
        this.player = Player.new(this.game, 'player')
        this.player.x = 200
        this.player.y = 700
        this.addElement('player', this.player)
    }

    addEnemies() {
        for (let i = 0; i < this.numberOfEnemies; i++) {
            var e = Enemy.new(this.game)
            this.addElement('enemies', e)
        }
    }

    filterElements(type, callback) {
        this.elements[type]
            && (this.elements[type] = this.elements[type].filter(callback))
    }

    showRestartLabel() {
        var l = GuaLabel.new(this.game, "Press R to Restart")
        this.addElement('label', l)
    }

    update() {
        super.update()
        this.filterElements('bullet', b => !b.hit)
        this.filterElements('particleSystem', ps => !ps.completed)
        this.filterElements('enemy_bullet', b => !b.hit && !b.touchDown)
        this.filterElements('player', p => p.alive)
        if (!this.player.alive) {
            this.showRestartLabel()
        }
    }
}