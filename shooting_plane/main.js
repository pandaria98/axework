window.paused = false

var enableDebugMode = function(enable) {
    if (!enable) {
        return
    }
    window.addEventListener('keydown', function(event){
        k = event.key
        if (k == 'p') {
            // pause function
            log('paused')
            window.paused = !window.paused
        }
    })
}

var __main = function() {
    enableDebugMode(true)

    images = {
        bg: 'img/bg.jpg',
        bullet: 'img/bullet.png',
        player: 'img/player.png',
        stone0: 'img/stone0.png',
        enemy0: 'img/enemy0.png',
        enemy1: 'img/enemy1.png',
        enemy2: 'img/enemy2.png',
        enemy3: 'img/enemy3.png',
        enemy4: 'img/enemy4.png',
        particle: 'img/particle.png',
        enemyBullet: 'img/enemy_bullet.png',
    }
    var game = GuaGame.instance(images, SceneTitle)
}


__main()