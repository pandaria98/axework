var es = sel => document.querySelectorAll(sel)
var e = sel => document.querySelector(sel)

var bindAll = function(sel, eventName, callback) {
    var l = es(sel)
    for (let i = 0; i < l.length; i++) {
        const input = l[i];
        input.addEventListener(eventName, function(event){
            callback(event)
        })
    }
}

var templateControl = function(key, item) {
    var t = `
        <div>
            <label>
                <input class="gua-auto-slider" type="range"
                    value="${item.value}"
                    data-value="config.${key}"
                >
                ${item._comment}: <span class="gua-label">${item.value}</span>
            </label>
        </div>
    `
    return t
}

var insertControls = function() {
    var keys = Object.keys(config)
    for (var k of keys) {
        var div = e('.gua-controls')
        var item = config[k]
        var html = templateControl(k, item)
        div.insertAdjacentHTML('beforeend', html)
    }
}

var bindEvents = function() {
    bindAll(".gua-auto-slider", "input", function(event){
        var target = event.target
        var bindVar = target.dataset.value
        var v = target.value
        eval(bindVar + '.value =' + v)
        //
        var label = target.closest('label').querySelector('.gua-label')
        label.innerText = v
    })
}

var __control = function() {
    // Insert HTML view from config file
    insertControls()
    // Bind events to sliders
    bindEvents()
}

__control()