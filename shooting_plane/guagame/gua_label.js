class GuaLabel {
    constructor(game, text) {
        this.game = game
        this.text = text
    }

    static new(game, text) {
        return new this(game, text)
    }

    draw() {
        this.game.context.font = "bold 40px Arial"
        this.game.context.fillText(this.text, 50, 150)
    }

    update() {

    }
}