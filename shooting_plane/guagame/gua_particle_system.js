class GuaParticleSystem {
    constructor(game) {
        this.game = game
        this.setup()
    }

    static new(game) {
        return new this(game)
    }

    setup() {
        this.x = 150
        this.y = 200
        this.numberOfParticles = 50
        this.particles = []
        this.completed = false
    }

    draw() {
        for (const p of this.particles) {
            if (p.alive) {
                p.draw()
            }
        }
    }

    update() {
        // add new particle
        if (this.particles.length <= this.numberOfParticles) {
            var p = GuaParticle.new(this.game)
            p.x = this.x
            p.y = this.y
            p.vx = randomBetween(-5, 5)
            p.vy = randomBetween(-5, 5)
            this.particles.push(p)
        }

        // update all particles
        var numberOfDead = 0
        for (let i = 0; i < this.particles.length; i++) {
            const p = this.particles[i];
            p.update()
            if (p.life < 0) {
                numberOfDead += 1
            }
        }

        // if all particles are dead, mark it completed
        if (numberOfDead >= this.numberOfParticles) {
            this.completed = true
        }
    }
}


class GuaParticle extends GuaImage {
    constructor(game) {
        super(game, 'particle')
        this.setup()
    }

    setup() {
        this.life = 20
        this.alive = true
    }

    init(x, y, vx, vy) {
        this.x = x
        this.y = y
        this.vx = vx
        this.vy = vy
    }

    update() {
        this.life--
        if (this.life <= 0) {
            this.alive = false
        }

        this.x += this.vx
        this.y += this.vy

        var factor = 0.01
        this.vx -= factor * this.vx
        this.vy -= factor * this.vy
    }
}