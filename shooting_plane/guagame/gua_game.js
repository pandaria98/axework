class GuaGame {
    constructor(images, sceneClass) {
        window.fps = 60

        this.images = images
        this.sceneClass = sceneClass

        var canvas = document.querySelector("#id-canvas")
        var context = canvas.getContext('2d')
        this.canvas = canvas
        this.context = context

        this.actions = {}
        this.keydowns = {}

        // event registration system
        window.addEventListener("keydown", () => {
            this.keydowns[event.key] = true
        })
        window.addEventListener("keyup", () => {
            this.keydowns[event.key] = false
        })

        this.init()
    }

    textureByName(name) {
        var img = this.images[name]
        return img
    }


    static instance(...args) {
        this.i = this.i || new this(...args)
        return this.i
    }

    drawImage(guaImage) {
        this.context.drawImage(guaImage.texture, guaImage.x, guaImage.y)
    }

    registerAction(key, callback) {
        this.actions[key] = callback
    }

    update() {
        this.debug()
        if (window.paused) {
            return
        }
        this.scene.update()
    }

    debug() {
        window.fps = config.fps.value
    }

    draw() {
        this.scene.draw()
    }

    replaceScene(scene) {
        this.scene = scene
    }

    runLoop() {
        // events
        var registeredKeys = Object.keys(this.actions)
        for (let i = 0; i < registeredKeys.length; i++) {
            const k = registeredKeys[i];
            // if keydown, invoke registered action
            if (this.keydowns[k]) {
                this.actions[k]()
            }
        }

        // update
        this.update()

        // clear
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height)

        // draw
        this.draw()

        setTimeout(() => {
            this.runLoop()
        }, 1000/window.fps);
    }

    init() {
        var loads = []
        var names = Object.keys(this.images)
        for (let i = 0; i < names.length; i++) {
            const name = names[i];
            var path = this.images[name]
            let img = new Image()
            img.src = path
            img.onload = () => {
                // store in this.images
                this.images[name] = img
                // after all images loaded, invoke run
                loads.push(1)
                log('load image', loads.length, names.length)
                if (loads.length == names.length) {
                    log('load images', this.images)
                    this.__start()
                }
            }
        }
    }

    __start() {
        // after loaded all images, invoke runloop
        this.scene = this.sceneClass.new(this)
        // run loop driver
        setTimeout(() => {this.runLoop()}, 1000/fps)
    }

    getCanvasBoundingBox() {
        var box = {
            left: 0,
            right: this.canvas.width,
            top: 0,
            bottom: this.canvas.height,
        }
        return box
    }
}