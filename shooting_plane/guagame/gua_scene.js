class GuaScene {
    constructor(game) {
        this.debugModeEnabled = true
        this.game = game
        this.elements = []
    }

    static new(game) {
        var i = new this(game)
        return i
    }

    iterAllElements(callback) {
        var keys = Object.keys(this.elements)
        for (let i = 0; i < keys.length; i++) {
            const type = keys[i];
            for (let j = 0; j < this.elements[type].length; j++) {
                var e = this.elements[type][j]
                callback(e)
            }
        }
    }

    addElement(type, guaImage) {
        guaImage.scene = this
        !(this.elements[type]) && (this.elements[type] = [])
        this.elements[type].push(guaImage)
    }

    draw() {
        this.iterAllElements(e => e.draw())
    }

    update() {
        if (this.debugModeEnabled) {
            this.iterAllElements(e => {e.debug && e.debug()})
        }
        this.iterAllElements(e => e.update())
    }
}