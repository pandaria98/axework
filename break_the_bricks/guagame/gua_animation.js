class GuaAnimation {
    constructor(game, namesAndFrames) {
        this.game = game
        this.namesAndFrames = namesAndFrames
        this.setup()
    }

    static new(game) {
        return new this(game)
    }

    setup() {
        this.animations = {}
        for (const o of this.namesAndFrames) {
            this.animations[o.name] = []
            for (var i = 0; i < o.numberOfFrames; i++) {
                var name = o.name + i
                var t = this.game.textureByName(name)
                this.animations[o.name].push(t)
            }

        }

        this.flipX = false

        this.animationName = this.namesAndFrames[0].name
        this.frameIndex = 0
        this.frameCount = 5
        this.texture = this.frames()[0]
        this.w = this.texture.width
        this.h = this.texture.height
    }

    frames() {
        return this.animations[this.animationName]
    }

    update() {
        this.frameCount--
        if (this.frameCount <= 0) {
            this.frameIndex = (this.frameIndex + 1) % this.frames().length
            this.texture = this.frames()[this.frameIndex]
            this.frameCount = 5
        }
    }

    draw() {
        if (this.flipX) {
            var context = this.game.context

            context.save()

            var x = this.x + this.w / 2
            context.translate(2 * x, 0)
            context.scale(-1, 1)
            context.drawImage(this.texture, this.x, this.y)

            context.restore()
        } else {
            this.game.drawImage(this)
        }
    }

    changeAnimation(name) {
        this.animationName = name
    }

}