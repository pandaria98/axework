class GuaLabel {
    constructor(game, text) {
        this.game = game
        this.text = text
        this.x = 50
        this.y = 150
    }

    static new(game, text) {
        return new this(game, text)
    }

    draw() {
        this.game.context.font = "bold 40px Arial"
        this.game.context.fillText(this.text, this.x, this.y)
    }

    update() {

    }
}