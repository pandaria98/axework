window.paused = false

var enableDebugMode = function(enable) {
    if (!enable) {
        return
    }
    window.addEventListener('keydown', function(event){
        k = event.key
        if (k == 'p') {
            // pause function
            paused = !paused
        }
    })
    // control FPS
    document.querySelector('#id-input-speed').addEventListener('input', function(event){
        var input = event.target
        // log(event, input.value)
        input_number = Number(input.value)
        window.fps = input_number == 0 ? 1 : input_number
    })
}


var __main = function() {
    enableDebugMode(true)

    images = {
        paddle: 'img/paddle.png',
        block: 'img/block.png',
        ball: 'img/ball.png',
        border: 'img/block_border.png',
    }

    var game = GuaGame.instance(images, SceneTitle)
}


__main()