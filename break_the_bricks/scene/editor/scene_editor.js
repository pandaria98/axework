class SceneEditor extends Scene {
    constructor(game) {
        super(game)
    }
    
    setup() {
        super.setup()

        this.paddle.editing = true

        this.clearBlocks()
        this.addInvisibleBlocks()

        this.indicator = document.querySelector('#id-level-indicator')
        this.indicator.innerHTML = 'level = [<br />]'

        this.bindEvents()
    }

    bindEvents() {
        var game = this.game
        game.canvas.addEventListener('mousedown', event => {
            var x = event.offsetX
            var y = event.offsetY
            this.addBlock(x, y)
        })
    }

    addInvisibleBlocks() {
        var game = this.game
        var blocks = []
        for (let i = 0; i < 10; i++) {
            for (let j = 0; j < 10; j++) {
                var x = i * 40
                var y = j * 19
                var p = [x, y]
                var b = Block.new(game, p)
                b.alive = false
                this.addElement(b)
                blocks.push(b)

                // Add border to help recognize grids
                var border = GuaImage.new(game, 'border')
                border.x = x
                border.y = y
                this.addElement(border)
            }
        }
        this.blocks = blocks
    }

    addBlock(x, y) {
        for (let b of this.blocks) {
            if (pointIn(b, x, y)) {
                b.alive = !b.alive
                this.updateIndicator()
            }
        }
    }

    updateIndicator() {
        var s = ''
        for (let b of this.blocks) {
            if (b.alive) {
                s += `&nbsp;&nbsp;&nbsp;&nbsp;[${b.x}, ${b.y}],<br />`
            }
        }
        this.indicator.innerHTML = 'level = [<br />' + s +']'
    }
}