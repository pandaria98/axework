class SceneTitle extends GuaScene {
    constructor(game) {
        super(game)
        this.setup()
    }

    setup() {
        var game = this.game

        var labelStart = GuaLabel.new(game, "Press K to start")
        this.addElement(labelStart)

        var labelEditor = GuaLabel.new(game, "Press E to editor")
        labelEditor.y += 100
        this.addElement(labelEditor)

        game.registerAction('k', function(){
            var main = Scene.new(game)
            game.replaceScene(main)
        })
        game.registerAction('e', function(){
            var editor = SceneEditor.new(game)
            game.replaceScene(editor)
        })
    }

}