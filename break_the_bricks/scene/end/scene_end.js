class SceneEnd extends GuaScene {
    constructor(game) {
        super(game)
        this.setup()
    }

    setup() {
        var game = this.game

        var label = GuaLabel.new(game, 'Press R to restart')
        this.addElement(label)

        game.registerAction('r', function(){
            var title = SceneTitle.new(game)
            game.replaceScene(title)
        })
    }


    update() {

    }
}