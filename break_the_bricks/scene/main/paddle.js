class Paddle extends GuaImage {
    constructor(game) {
        super(game, 'paddle')
        this.setup()
    }

    setup() {
        this.x = 150
        this.y = 250
        this.speed = 10

        this.editing = false
    }

    move(x) {
        if (x < 0) {
            x = 0
        }
        if (x > 400 - this.w) {
            x = 400 - this.w
        }
        this.x = x
    }

    moveLeft() {
        this.move(this.x - this.speed)
    }

    moveRight() {
        this.move(this.x + this.speed)
    }

    collide(ball) {
        return rectIntersect(this, ball)
    }

    miss(ball) {
        return ball.y > this.y
    }

    update() {
        var b = this.scene.ball
        if (this.collide(b)) {
            b.bounce()
        }

        if (this.miss(b)) {
            // In editor, ball should not die
            if (this.editing) {
                return
            }
            var s = this.scene
            var end = SceneEnd.new(s.game)
            s.game.replaceScene(end)
        }
    }
}