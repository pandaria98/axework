class Ball extends GuaImage {
    constructor(game) {
        super(game, 'ball')
        this.setup()
    }

    setup() {
        this.x = 150
        this. y = 200
        this.speedX = 5
        this.speedY = 5
        this.fired = false
    }

    fire() {
        this.fired = true
    }

    move() {
        if (this.fired) {
            if (this.x < 0 || this.x > 400) {
                this.speedX *= -1
            }
            if (this.y < 0 || this.y > 300) {
                this.speedY *= -1
            }
            this.x += this.speedX
            this.y += this.speedY
        }
    }

    bounce() {
        this.speedY *= -1
    }

    update() {
        this.move()
    }
}