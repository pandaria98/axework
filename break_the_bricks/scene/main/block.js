class Block extends GuaImage {
    constructor(game, position) {
        super(game, 'block')
        this.setup(position)
    }

    setup(position) {
        // position is formatted with [0, 0]
        var p = position
        this.x = p[0]
        this.y = p[1]
        this.alive = true
        this.lives = p[2] || 1
    }

    collide(ball) {
        return this.alive && rectIntersect(this, ball)
    }

    kill() {
        this.lives--
        if (this.lives < 1) {
            this.alive = false
        }
    }

    update() {
        var b = this.scene.ball
        if (this.collide(b)) {
            b.bounce()
            this.kill()
        }
    }

    draw() {
        // log('alive', this.alive)
        if (this.alive) {
            // log('draw')
            super.draw()
        }
    }
}