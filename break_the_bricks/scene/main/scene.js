class Scene extends GuaScene {
    constructor(game) {
        super(game)
        this.setup()
    }

    setup() {
        var game = this.game
        this.score = 0

        this.ball = Ball.new(game)
        this.addElement(this.ball)

        this.paddle = Paddle.new(game)
        this.addElement(this.paddle)

        this.loadLevel(3)

        game.registerAction('a', () => this.paddle.moveLeft())
        game.registerAction('d', () => this.paddle.moveRight())
        game.registerAction('f', () => this.ball.fire())
        game.registerAction('1', () => this.loadLevel(1))
        game.registerAction('2', () => this.loadLevel(2))
        game.registerAction('3', () => this.loadLevel(3))

        this.addDragListener()
    }

    loadLevel(n) {
        this.blocks && this.clearBlocks()

        n = n - 1
        var level = levels[n]
        var blocks = []
        for (let i = 0; i < level.length; i++) {
            var p = level[i]
            var b = Block.new(this.game, p)
            blocks.push(b)
            this.addElement(b)
        }
        this.blocks = blocks
    }

    clearBlocks() {
        // log('blocks clear')
        var blocks = this.blocks
        for (let b of blocks) {
            this.removeElement(b)
        }
    }

    addDragListener() {
        var game = this.game
        game.canvas.addEventListener('mousedown', event => {
            var x = event.offsetX
            var y = event.offsetY
            if (pointIn(this.ball, x, y)) {
                this.enableDrag = true
            }
        })
        game.canvas.addEventListener('mousemove', event => {
            var x = event.offsetX
            var y = event.offsetY
            if (this.enableDrag) {
                this.ball.x = x
                this.ball.y = y
            }
        })
        game.canvas.addEventListener('mouseup', event => {
            var x = event.offsetX
            var y = event.offsetY
            if (this.enableDrag) {
                this.enableDrag = false
            }
        })
    }
}