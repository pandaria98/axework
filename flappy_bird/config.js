const config = {
    pipe_vertical_space: {
        _comment: "管子垂直间距",
        value: 200,
    },
    pipe_landscape_space: {
        _comment: "管子水平间距",
        value: 400,
    },
}