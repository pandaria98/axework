window.paused = false

var enableDebugMode = function(enable) {
    if (!enable) {
        return
    }
    window.addEventListener('keydown', function(event){
        k = event.key
        if (k == 'p') {
            // pause function
            paused = !paused
        }
    })
    // control FPS
    // log(document.querySelector('#id-input-speed'))
    document.querySelector('#id-input-speed').addEventListener('input', function(event){
        var input = event.target
        // log(event, input.value)
        input_number = Number(input.value)
        window.fps = input_number == 0 ? 1 : input_number
    })
}

var __main = function() {
    enableDebugMode(true)

    images = {
        fly0: 'bird/fly1.png',
        fly1: 'bird/fly2.png',
        bg: 'bird/background.png',
        ground: 'bird/ground.png',
        pipeTop: 'bird/pipe_top.png',
        pipeBottom: 'bird/pipe_bottom.png',
    }
    var game = GuaGame.instance(images, SceneTitle)
}


__main()