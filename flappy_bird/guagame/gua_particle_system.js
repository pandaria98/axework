class GuaParticleSystem {
    constructor(game) {
        this.game = game
        this.setup()
    }

    static new(game) {
        return new this(game)
    }

    setup() {
        this.x = 150
        this.y = 200
        this.numberOfParticles = 100
        this.particles = []
    }

    draw() {
        for (const p of this.particles) {
            p.draw()
        }
    }

    update() {
        // add new particle
        if (this.particles.length <= this.numberOfParticles) {
            var p = GuaParticle.new(this.game)
            p.x = this.x
            p.y = this.y
            p.vx = randomBetween(-10, 10)
            p.vy = randomBetween(-10, 10)
            this.particles.push(p)
        }

        // update all particles
        for (const p of this.particles) {
            p.update()
        }

        // delete dead particles
        this.particles = this.particles.filter(p => p.life > 0)
    }
}


class GuaParticle extends GuaImage {
    constructor(game) {
        super(game, 'particle')
        this.setup()
    }

    setup() {
        this.life = 20
    }

    init(x, y, vx, vy) {
        this.x = x
        this.y = y
        this.vx = vx
        this.vy = vy
    }

    update() {
        this.life--
        this.x += this.vx
        this.y += this.vy

        var factor = 0.01
        this.vx -= factor * this.vx
        this.vy -= factor * this.vy
    }
}