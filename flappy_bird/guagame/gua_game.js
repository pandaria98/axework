class GuaGame {
    constructor(images, sceneClass) {
        window.fps = 60

        this.images = images
        this.sceneClass = sceneClass

        var canvas = document.querySelector("#id-canvas")
        var context = canvas.getContext('2d')
        this.canvas = canvas
        this.context = context

        this.actions = {}
        this.keyStatus = {}

        // event registration system
        window.addEventListener("keydown", () => {
            this.keyStatus[event.key] = 'down'
        })
        window.addEventListener("keyup", () => {
            this.keyStatus[event.key] = 'up'
        })

        this.init()
    }

    textureByName(name) {
        var img = this.images[name]
        return img
    }


    static instance(...args) {
        this.i = this.i || new this(...args)
        return this.i
    }

    drawImage(guaImage) {
        this.context.drawImage(guaImage.texture, guaImage.x, guaImage.y)
    }

    registerAction(key, callback) {
        this.actions[key] = callback
    }

    update() {
        if (window.paused) {
            return
        }
        this.scene.update()
    }

    draw() {
        this.scene.draw()
    }

    replaceScene(scene) {
        this.scene = scene
    }

    runLoop() {
        // events
        var registeredKeys = Object.keys(this.actions)
        for (let i = 0; i < registeredKeys.length; i++) {
            const k = registeredKeys[i];
            var status = this.keyStatus[k]
            // if key status changed, invoke callback
            if (status == 'down') {
                this.actions[k]('down')
            } else if (status == 'up') {
                this.actions[k]('up')
                this.keyStatus[k] = null
            }
        }

        // update
        this.update()

        // clear
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height)

        // draw
        this.draw()

        setTimeout(() => {
            this.runLoop()
        }, 1000/window.fps);
    }

    init() {
        var loads = []
        var names = Object.keys(this.images)
        for (let i = 0; i < names.length; i++) {
            const name = names[i];
            var path = this.images[name]
            let img = new Image()
            img.src = path
            img.onload = () => {
                // store in this.images
                this.images[name] = img
                // after all images loaded, invoke run
                loads.push(1)
                // log('load image', loads.length, names.length)
                if (loads.length == names.length) {
                    // log('load images', this.images)
                    this.__start()
                }
            }
        }
    }

    __start() {
        // after loaded all images, invoke runloop
        this.scene = this.sceneClass.new(this)
        // run loop driver
        setTimeout(() => {this.runLoop()}, 1000/window.fps)
    }
}