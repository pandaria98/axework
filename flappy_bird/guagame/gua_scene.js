class GuaScene {
    constructor(game) {
        this.debugModeEnabled = true
        this.game = game
        this.elements = []
    }

    static new(game) {
        var i = new this(game)
        return i
    }

    addElement(guaImage) {
        guaImage.scene = this
        this.elements.push(guaImage)
    }

    draw() {
        for (let i = 0; i < this.elements.length; i++) {
            var e = this.elements[i]
            e.draw()
        }
    }

    update() {
        if (this.debugModeEnabled) {
            for (let i = 0; i < this.elements.length; i++) {
                var e = this.elements[i]
                e.debug && e.debug()
            }
        }
        for (let i = 0; i < this.elements.length; i++) {
            var e = this.elements[i]
            e.update()
        }
    }
}