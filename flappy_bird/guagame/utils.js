var log = console.log.bind(console)

var imageFromPath = function(path) {
    var img = new Image()
    img.src = path
    return img
}

var rectIntersect = function(a, b) {
    var getBoundingBox = function(a) {
        o = {
            left: a.x,
            top: a.y,
            right: a.x + a.w,
            bottom: a.y + a.h,
        }
        return o
    }
    a_box = getBoundingBox(a)
    b_box = getBoundingBox(b)
    
    nonIntersect = (a_box.right < b_box.left)
                    || (a_box.left > b_box.right)
                    || (a_box.bottom < b_box.top)
                    || (a_box.top > b_box.bottom)
    intersect = !nonIntersect
    return intersect
}

var pointIn = function(object, x, y) {
    xIn = x > object.x && x < object.x + object.image.width
    yIn = y > object.y && y < object.y + object.image.height
    return xIn && yIn
}

const randomBetween = function(start, end) {
    var n = Math.random() * (end - start + 1)
    return Math.floor(n + start)
}