class SceneTitle extends GuaScene {
    constructor(game) {
        super(game)
        this.setup()
    }

    setup() {
        var game = this.game
        var bg = GuaImage.new(game, 'bg')
        this.addElement(bg)

        var ground = Ground.new(game)
        this.addElement(ground)

        var bird = Bird.new(game)
        bird.x = 250
        bird.y = 450
        bird.falling = false
        this.addElement(bird)

        var prompt = GuaLabel.new(game, 'press J to jump')
        prompt.y = 700
        this.addElement(prompt)

        game.registerAction('j', function(){
            var main = Scene.new(game)
            game.replaceScene(main)
        })
    }
}