class Scene extends GuaScene {
    constructor(game) {
        super(game)
        this.setup()
    }

    setup() {
        var game = this.game
        var bg = GuaImage.new(game, 'bg')
        this.addElement(bg)

        this.pipes = Pipes.new(game)
        this.addElement(this.pipes)

        var ground = Ground.new(game)
        this.addElement(ground)

        this.bird = Bird.new(game)
        this.bird.x = 250
        this.bird.y = 450
        this.addElement(this.bird)

        this.score = 0
        this.scoreLabel = GuaLabel.new(game, String(this.score))
        this.scoreLabel.x = 250
        this.addElement(this.scoreLabel)

        this.setupInputs()
    }

    debug() {
    }

    setupInputs() {
        this.game.registerAction('j', (keyStatus) => this.bird.jump(keyStatus))
        this.game.registerAction('r', (keyStatus) => {
            if (!this.bird.alive) {
                var st = SceneTitle.new(this.game)
                this.game.replaceScene(st)
            }
        })
    }

    update() {
        if (this.bird.alive) {
            super.update()
            this.scoreLabel.text = this.score
        } else {
            this.bird.update()
        }
    }
}