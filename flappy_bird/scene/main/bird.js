class Bird extends GuaAnimation {
    constructor(game) {
        super(game, [{name:'fly', numberOfFrames:2}])
        this.setup()
    }

    setup() {
        super.setup()
        this.ay = 6 * 0.2
        this.vy = 0
        this.rotatoin = 0
        this.falling = true
        this.alive = true
        this.jumped = false

        this.horizonHeight = 752
    }

    accelerate() {
        this.y += this.vy
        this.vy += this.ay
    }

    rotate() {
        if (!this.alive) {
            this.rotation = 45
        } else if (this.rotation < 45) {
            this.rotation += 3
        }
    }

    keepOverGround() {
        var h = this.horizonHeight
        if (this.y > h) {
            this.y = h
        }
    }

    update() {
        if (this.alive) {
            super.update()
        }
        if (this.falling) {
            this.accelerate()
            this.rotate()
            this.keepOverGround()
        }
    }

    stopRising() {
        this.vy = this.vy > 0 ? 0 : this.vy
    }

    die() {
        this.alive = false
        this.stopRising()
        
        var prompt = GuaLabel.new(this.game, 'Press R to Restart')
        prompt.y = 700
        this.scene.addElement(prompt)
    }

    jump(keyStatus) {
        if (!this.alive) {
            return
        }
        if (keyStatus == 'down' && !this.jumped) {
            this.jumped = true
            this.vy = -15
            this.rotation = -45
        } else if (keyStatus == 'up') {
            this.jumped = false
        }
    }

    draw() {
        var w2 = this.w / 2
        var h2 = this.h / 2

        var context = this.game.context
        context.save()

        context.translate(this.x + w2, this.y + h2)
        if (this.flipX) {
            context.scale(-1, 1)
        }
        context.rotate(this.rotation * Math.PI / 180)
        context.translate(-w2, -h2)
        context.drawImage(this.texture, 0, 0)

        context.restore()
    }
}