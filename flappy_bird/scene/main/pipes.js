class Pipes {
    constructor(game) {
        this.game = game
        this.setup()
    }

    static new(game) {
        return new this(game)
    }

    setup() {
        this.pipes = []
        this.pipesSpace = 300
    }

    addPipe() {
        var p = SinglePipe.new(this.game)
        p.scene = this.scene
        this.pipes.push(p)
    }

    draw() {
        for (let i = 0; i < this.pipes.length; i++) {
            const p = this.pipes[i];
            p.draw()
        }
    }

    removePipesOut() {
        this.pipes.filter(p => p.x + p.w > 0)
    }

    intervalEnough() {
        var lastPipe = this.pipes[this.pipes.length - 1]
        var deadLineX = this.game.canvas.width - this.pipesSpace
        return lastPipe.x < deadLineX
    }

    update() {
        // HACK, for pass scene reference to single pipe, have to add first
        // pipe in this way
        if (this.pipes.length == 0) {
            this.addPipe()
        }

        // Update all pipes
        for (let i = 0; i < this.pipes.length; i++) {
            const p = this.pipes[i];
            p.update()
        }

        if (this.intervalEnough()) {
            this.addPipe()
        }

        this.removePipesOut()
    }
}

class SinglePipe {
    constructor(game) {
        this.game = game
        this.setup()
    }

    static new(game) {
        return new this(game)
    }

    setup() {
        var game = this.game
        this.pipeTop = GuaImage.new(game, 'pipeTop')
        this.pipeBottom = GuaImage.new(game, 'pipeBottom')

        this.verticalSpace = 200
        
        var x = this.game.canvas.width
        this.setX(x)

        var y = randomBetween(-400, -100)
        this.setY(y)

        this.w = this.pipeTop.w
        this.h = this.pipeTop.h + this.verticalSpace + this.pipeBottom.h 

        this.birdInside = false
    }

    setX(x) {
        this.x = x
        this.pipeTop.x = x
        this.pipeBottom.x = x
    }

    setY(y) {
        this.y = y
        this.pipeTop.y = y
        this.pipeBottom.y = this.pipeTop.y + this.pipeTop.h + this.verticalSpace
    }

    draw() {
        this.game.drawImage(this.pipeTop)
        this.game.drawImage(this.pipeBottom)
    }

    collide(bird) {
        var b = bird
        var top = this.pipeTop
        var bottom = this.pipeBottom
        return rectIntersect(top, b) || rectIntersect(bottom, b)
    }

    gapCollide(bird) {
        var top = this.pipeTop
        var bottom = this.pipeBottom
        var gap = {
            x: top.x,
            y: top.y + top.h,
            w: top.w,
            h: bottom.y - (top.y + top.h),
        }
        return rectIntersect(gap, bird)
    }

    update() {
        var top = this.pipeTop
        var bottom = this.pipeBottom

        var x = this.x - 5
        this.setX(x)

        var b = this.scene.bird
        if (this.collide(this.scene.bird)) {
            b.die()
        }

        // When bird get into gap, mark birdInside true
        if (this.gapCollide(b) && !this.birdInside) {
            this.birdInside = true
        }

        // If bird was inside the gap and bird jump through the pipes, score
        var right = this.x + this.w
        if (this.birdInside && b.x > right) {
            this.birdInside = false
            this.scene.score += 1
            log('score')
        }
    }
}