class Ground extends GuaImage {
    constructor(game) {
        super(game, 'ground')
        this.setup()
    }

    setup() {
        this.x = 0
        this.y = 800
    }

    update() {
        // move ground
        // log('ground move', this.ground)
        this.x -= 4
        if (this.x < -28) {
            this.x = 0
        }
    }
}